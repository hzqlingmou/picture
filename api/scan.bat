@echo off
setlocal enabledelayedexpansion

:: 定义输出文件以及基础URL
set "output_file=images.txt"
set "base_url=https://pi.escaped.icu/api/pc/"

:: 检查是否提供了参数（即要扫描的目录）
if "%~1"=="" (
    echo Usage: %~nx0 "path_to_scan"
    exit /b 1
)

:: 设置要扫描的目录
set "scan_directory=%~f1"

:: 清空或创建输出文件
> "%output_file%" echo.

:: 遍历指定目录及其子目录中的所有图片文件
for /r "%scan_directory%" %%f in (*.png *.jpg *.jpeg *.gif *.bmp *.svg) do (
    :: 获取文件相对于扫描目录的相对路径
    set "full_path=%%~ff"
    
    :: 计算相对路径并替换反斜杠为正斜杠
    call :get_relative_path "!full_path!" "%scan_directory%"
    
    :: 构建新URL并写入到输出文件
    echo !base_url!!relative_path! >> "%output_file%"
)

echo All image paths have been written to %output_file%
goto :eof

:get_relative_path
    set "full_path=%~1"
    set "scan_directory=%~2"
    
    :: 去除扫描目录部分
    set "relative_path=!full_path:%scan_directory%=!"
    
    :: 移除任何开头的反斜杠
    if "!relative_path:~0,1!"=="\" set "relative_path=!relative_path:~1!"
    
    :: 替换所有反斜杠为正斜杠
    set "relative_path=!relative_path:\=/!"
    
    :: 返回计算的相对路径
    exit /b